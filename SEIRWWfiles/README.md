
## Description of main functions

### _SEIRWWinit_

Estimates the weekly cycle of expected cases (output vector `C`), and generates x-ticks (output `firsts`) and their labels (output `labs`) for plots.

Inputs: 
- `YC`: Vector of daily cases
- `day1`: The date of the first data point (format DD/MM/YYYY)
- `specialHolidays`: Indices of irregular days with reduced case numbers due to holidays (for example)
- `darkNumber`: Ratio of total and detected cases (format specified above)

### _SEIRWWcalibrate_

Sets parameters to a strucutre `params` that are common to all regions (alpha, beta(0), tau, kappa, q<sub>beta</sub>) and estimates region-specific parameters (initial compartment sizes E(0) and I(0), and gamma, epsilon, nu, and U<sub>W</sub>).

Inputs:

- `YC`: Vector of daily cases
- `YW`: Vector of wastewater data
- `C`: Expectation of daily share of detected cases (from _SEIRWWinit_)
- `params`: Initial parameters containing a field `params.N` for the population size

### _SEIR_WW_

The main file for the SEIR-WW-EKF implementation.

Inputs:

- `params`: model parameters obtained by calibration
- `YC`: Vector of daily cases
- `YW`: Vector of wastewater data
- `C`: Expectation of daily share of detected cases (from _SEIRWWinit_)
- `useData`: Indicator for which data the EKF uses for the state update (logical of size two. First entry indicates whether case data is used, and second entry indicates whether wastewater data is used)
- `maxind`: vector of indices at which the state estimate is stored. The largest index indicates the day until which simulation is done.
- `firsts` and `labs`: ticks and tick labels for plots
-`pl`: logical indicator for whether plots are shown or not

Outputs:

- `Yest`: Estimated output. First line gives the estimated case data and second line gives the estimated wastewater data.
- `Xend`: State estimates stored at times indicated by the `maxind` input
- `Reff`: The estimated effective reproduction number
- `err_beta`: standard deviation for the beta(t) parameter estimate

### _SEIR_WW_FWD_

SEIR-WW model without state corrections by the EKF. It is used for projections forward in time.

Inputs:

- `X`: Initial state vector
- `C`: Expectation of daily share of detected cases (from _SEIRWWinit_)
- `P`: The error covariance matrix for the initial state
- `initDay`: The index of starting date of the simulation (used for matching `C`)
- `params`: model parameters obtained by calibration
- `maxind`: Simulation time length

Outputs:

- `Yest`: Estimated output. First line gives the estimated case data and second line gives the estimated wastewater data
- `err`: Standard deviation for the daily case number projections

### _SEIRreaction_

The reaction function (output `R`) and its Jacobian (output `JR`), that is, a vector of transition rates between different states as a function of the current state.

Inputs:

- `X`: the current state
- `N`, `alpha`, `tau1`, `gamma`, `nu`, `eta`: model parameters
- `dt`: time discretization step

### _paramFit_

The cost function that _SEIRWWcalibrate_ uses for parameter fitting.

### _RWest_

Calculates `RW` (U<sub>W</sub> in the manuscript) as described in the manuscript.





