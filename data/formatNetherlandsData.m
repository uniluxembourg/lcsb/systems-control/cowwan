

%Create dates for data extraction and for the standardised data table
dates={'2020-02-29'};
datesout = {'29/02/2020'};
monthLengths = [31 28 31 30 31 30 31 31 30 31 30 31];
for jm = 3:12 %2020
    for jd = 1:monthLengths(jm)
        mm = num2str(jm);
        if length(mm) == 1
            mm = ['0' mm];
        end
        dd = num2str(jd);
        if length(dd) == 1
            dd = ['0' dd];
        end
        
        dates={dates{1:length(dates)},['2020-' mm '-' dd]};
        datesout={datesout{1:length(datesout)},[dd '/' mm '/2020']};
    end
end
for jm = 1:12 %2021
    for jd = 1:monthLengths(jm)
        mm = num2str(jm);
        if length(mm) == 1
            mm = ['0' mm];
        end
        dd = num2str(jd);
        if length(dd) == 1
            dd = ['0' dd];
        end
        
        dates={dates{1:length(dates)},['2021-' mm '-' dd]};
        datesout={datesout{1:length(datesout)},[dd '/' mm '/2021']};
    end
end

dates = dates(192:end);
TTW = readtable('Netherlands_ww_municipality.csv','Format','%s%s%s%s%n%n');

YW = zeros(1,length(dates));
dats = TTW.Date_measurement;
WWmeas = TTW.RNA_flow_per_100000;

%Calculate the average of measurements for each day
for jd = 1:length(dates)
    inds = find(strcmp(dats,dates{jd}));
    YW(jd) = mean(WWmeas(inds));
    
end
YW(isnan(YW)) = -1;
YW = YW(1:331);

%Read case data
TTC = readtable('Netherlands_cases_national.csv');
YC = TTC.positive;
YC = YC(230:end)';



%Netherlands data starts from 1/9/2020
datesout = datesout(192:end);
ddout = datetime(datesout,'InputFormat','dd/MM/yyyy');
TTout = table(ddout(1:331)',YC',YW','VariableNames',{'date','cases','ww'});
writetable(TTout,'Netherlands.xlsx')


