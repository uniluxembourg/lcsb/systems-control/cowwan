%% Choose the region by running the corresponding section below

%% Barcelona

clear('params')
params.region = 'Barcelona';
params.N = 2000000;
params.darkNumber = [1.8 1];
specialHolidays = [172 179 184]; %Christmas, new year
startDate = '24/08/2020';

%% Kitchener

clear('params')
params.region = 'Kitchener';
params.N = 242000;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '11/01/2021';

%% Kranj

clear('params')
params.region = 'Kranj';
params.N = 40000;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '10/09/2020';

%% Lausanne

clear('params')
params.region = 'Lausanne';
params.N = 240000;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '01/10/2020';

%% Ljubljana

clear('params')
params.region = 'Ljubljana';
params.N = 280000;
params.darkNumber = [1.8 1];
specialHolidays = [109 116 154];
startDate = '10/09/2020';

%% Luxembourg

clear('params')
params.region = 'Luxembourg';
params.N = 634730;
params.darkNumber = [3 1; 1.8 95]; %Number, used starting from 
specialHolidays = [46 84 95 117 302 303 304 309 310 403 429 441 452 482];
startDate = '26/02/2020';

%% Milwaukee

clear('params')
params.region = 'Milwaukee';
params.N = 615934;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '25/08/2020';

%% Netherlands

clear('params')
params.region = 'Netherlands';
params.N = 17178109; %from worldofmeters
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '07/09/2020';

%% Oshkosh

clear('params')
params.region = 'Oshkosh';
params.N = 68000;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '15/09/2020';

%% Raleigh

clear('params')
params.region = 'Raleigh';
params.N = 460000;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '03/01/2021';

%% Riera de la Bisbal

clear('params')
params.region = 'Riera_de_la_bisbal';
params.N = 100000;
params.darkNumber = [1.8 1];
specialHolidays = [180]; %New year
startDate = '06/07/2020';

%% Zurich

clear('params')
params.region = 'Zurich';
params.N = 450000;
params.darkNumber = [1.8 1];
specialHolidays = [];
startDate = '01/10/2020';

%% Import data, calibrate model, and save parameters and data together

addpath('../SEIRWWfiles/')
TT = readtable(['../data/' params.region '.xlsx']);
YC = TT.cases';
YW = TT.ww';

%Determine c_t and plot label dates
[C, labs, firsts, longDates] = SEIRWWinit(YC,startDate,specialHolidays,params.darkNumber);

%Case data from Raleigh is scaled (per 10k population) and then rounded. 
%Revert the scaling, and introduce an additional error term to W_c. In 
%addition, due to the rounding, there is a long period with zero cases 
%leading to failure in the estimation of C. Fix that here.
if strcmp(params.region,'Raleigh')
    params.Radditional = (params.N/20000)^2;
    C(171:195) = 1/params.darkNumber;
end

%Scale up WW data for some regions to have it on a reasonable scale
if max(strcmp(params.region,{'Kitchener','Kranj','Ljubljana'}))
    YW = 1e8*YW;
end

%Kitchener WW data has a jump on mid-May. Scaling later data improves
%performance.
% if strcmp(params.region,'Kitchener')
%     YW(127:end) = YW(127:end)*.4;
% end


% Calibrate the model and save parameters
params = SEIRWWcalibrate(YC,YW,C,params);
save(['../parameters/params_' params.region '.mat'],'params','C','YC','YW','labs','firsts')




