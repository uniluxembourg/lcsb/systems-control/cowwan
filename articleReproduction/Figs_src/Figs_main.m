%% Generate figures for CoWWAn (SEIR_WW_EKF) paper, main text

% The code has been developed and maintained by Atte Aalto, Daniele
% Proverbio and Francoise Kemp
% Affiliation: Luxembourg Centre for Systems Biomedicine
% Year: 2021
% References: "Model-based assessment of COVID-19 epidemic dynamics by
% wastewater sampling"

clc;
close all;
clear;

%% Global variables

% Considered countries
names = ["Barcelona PdL";"Kitchener";"Kranj";"Lausanne";"Ljubljana";"Luxembourg";"Milwaukee";"Netherlands";"Oshkosh";"Raleigh";"Riera de la Bisbal";"Zurich"]; 
acronym = ["BPL";"KIT";"KRA";"LAU";"LJU";"LUX";"MIL";"NLD";"OSH";"RAL";"RDB";"ZUR"];
population = [2000000;242000;40000;240000;280000;610000;615934;17280000;78300;460000;100000;450000];

% Parent directory
[parentdir,~,~]=fileparts(pwd);

% Data input
names1 = ["Barcelona";"Kitchener";"Kranj";"Lausanne";"Ljubljana";"Luxembourg";"Milwaukee";"Netherlands";"Oshkosh";"Raleigh";"Riera_de_la_Bisbal";"Zurich"]; 

% Prediction outputs
file_list_preds_7 = ["pred7days_Barcelona";"pred7days_Kitchener";"pred7days_Kranj";"pred7days_Lausanne";"pred7days_Ljubljana";"pred7days_Luxembourg";"pred7days_Milwaukee";"pred7days_Netherlands";"pred7days_Oshkosh";"pred7days_Raleigh";"pred7days_Riera_de_la_bisbal";"pred7days_Zurich"];
file_list_preds_14 = ["pred14days_Barcelona";"pred14days_Kitchener";"pred14days_Kranj";"pred14days_Lausanne";"pred14days_Ljubljana";"pred14days_Luxembourg";"pred14days_Milwaukee";"pred14days_Netherlands";"pred14days_Oshkosh";"pred14days_Raleigh";"pred14days_Riera_de_la_bisbal";"pred14daysrich"];

% Reconstruction outputs
file_list_rec = ["reconstruction_Barcelona"; "reconstruction_Kitchener"; "reconstruction_Kranj"; "reconstruction_Lausanne"; "reconstruction_Ljubljana"; "reconstruction_Luxembourg"; "reconstruction_Milwaukee"; "reconstruction_Netherlands"; "reconstruction_Oshkosh"; "reconstruction_Raleigh";"reconstruction_Riera_de_la_bisbal";"reconstruction_Zurich"];

c = colormap(); 

%% Fig 1a (model scheme)
% Model scheme created with Inkscape


%% Fig 1b (Example of reconstruction) 
% For Luxembourg

comboPlot(parentdir)

%% Fig 1c (Reconstruction results)

plotId = 0;

fig = figure('position',[10 10 1100 350]) ;

p = panel();
p.pack(2, 6);
p.de.margin = 12;
p.de.marginbottom = 6;
p.de.marginleft = 5;
p.margin = [15 15 5 5];

 for plotIdR = 1 : 2
     for plotIdC = 1 : 6
        plotId = plotId+1;
        p(plotIdR,plotIdC).select();
        reconstr = load([parentdir + "/reconstructionResults/"+ file_list_rec(plotId) + ".mat"]); 
        xxx = linspace(1,  max(reconstr.YC)) ;
        hold on
        plot(xxx,xxx,'--','color',[128 128 128]/256,'LineWidth',0.8)
        plot((reconstr.YC),(reconstr.caseEstimate),'.','MarkerEdgeColor',c(plotId,:),'MarkerFaceColor',c(plotId,:))
        title(names(plotId),'fontweight','normal','fontsize',12)
        axis square
        set(gca, 'XTick', [0,ceil(max(reconstr.YC)/(2*10))*10,ceil(max(reconstr.YC)/10)*10])
        xlim([0, ceil(max(reconstr.YC)/10)*10 + (ceil(max(reconstr.YC)/10)*10)*0.05])
        box on
        hold off
     end
 end
p.ylabel('Reconstructed case numbers');
p.xlabel('Case numbers');
set(gca,'FontSize',14)


%% Fig 1d (Plots of correlation coefficients)
% Comparing outputs from our method and from regression studies
% (already after data smoothing, to reduce noise)
% Input data from outputs of the main code
% Both raw-wastewater-based reconstruction and interpolated-wastewater-based 
% reconstructions are considered

%Correlations of smoothed data obtained here (corrSmoothedData)
%corrSmoothedData = [0.4709  0.5381  0.5882  0.7323  0.6694  0.8354  0.9407  0.5441  0.6204  0.8333  0.3866  0.7564];
run('../corAnalysis.m')

%Reconstruction correlations are obtained by running the corresponding
%sections in the articleReproduction/main.m file for each region
corrReconstruction = [0.6986, 0.7400, 0.8027, 0.8865, 0.8722, 0.9117, 0.9475, 0.6394, 0.7303, 0.9043, 0.3642, 0.8526];
corrIP = [.7757 .6611 .8168 .8227 .8747 .9221 .9486 .6378 .6756 .8934 .4181 .8519]; % from recontructed data, with interpolation of WW

figure('position',[10 10 1100 230])
p = panel();
p.pack(1);
p.de.margin = 12;
p.de.marginbottom = 6;
p.de.marginleft = 5;
p.margin = [15 15 5 5];
p(1).select()
hold on
plot(corrSmoothedData,'o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',10)
plot(corrReconstruction,'s','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',10)
plot(corrIP,'^','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',10)
xtickangle(15)
set(gca, 'XLim', [1 length(names)]);
set(gca, 'XTick', [1:length(names)]);
set(gca, 'XTickLabel', names);
set(gca, 'fontsize', 12);
xlim([0, length(names)+1])
ylim([0,1])
grid on
%legend(["Measured wastewater data" + newline + "vs measured case numbers","Reconstructed case numbers" + newline + "vs measured case numbers","Reconstructed case numbers" + newline + "vs measured case numbers (with interpolated ww)"],'Location','NorthOutside','Orientation','horizontal','Box','off','fontsize',11) %,'position',[1 0.35 0.05 0.07])
legend(["Measured wastewater data vs detected cases","Reconstructed cases vs detected cases","Reconstructed cases (with interpolated ww) vs detected cases"],'Location','NorthOutside','Orientation','horizontal','Box','off','fontsize',11) %,'position',[1 0.35 0.05 0.07])
p.ylabel("Correlation coefficient \rho")
set(gca,'FontSize',14)

%% Fig 2a (Example time series)
% Of 7-days projections

k=6;
Lux = load([parentdir + "/results/"+ file_list_preds_7(k) + ".mat"]); 
Lux_dates_WW = datetime(["2020-02-24",'2020-03-11','2020-03-30','2020-04-05','2020-04-16','2020-04-22','2020-04-28','2020-05-04','2020-05-10','2020-05-21','2020-05-27','2020-06-02','2020-06-08','2020-06-14','2020-06-25','2020-07-01','2020-07-07','2020-07-13','2020-07-19','2020-07-30','2020-08-05','2020-08-11','2020-08-17','2020-08-23','2020-09-03','2020-09-09','2020-09-15','2020-09-21','2020-09-27','2020-10-08','2020-10-14','2020-10-20','2020-10-26','2020-10-28','2020-11-01','2020-11-03','2020-11-05','2020-11-8','2020-11-10','2020-11-12','2020-11-16','2020-11-18','2020-11-22','2020-11-24','2020-11-26','2020-11-30','2020-12-02','2020-12-06','2020-12-09','2020-12-13','2020-12-15','2020-12-17','2020-12-22','2020-12-28','2021-01-04','2021-01-07','2021-01-11','2021-01-13','2021-01-17','2021-01-19','2021-01-25','2021-01-28','2021-01-31','2021-02-08','2021-02-11','2021-02-15','2021-02-17','2021-02-21','2021-02-23','2021-03-01','2021-03-04','2021-03-07','2021-03-11','2021-03-15','2021-03-18','2021-03-21','2021-03-24','2021-03-30','2021-04-01','2021-04-05','2021-04-07','2021-04-11','2021-04-13','2021-04-19','2021-04-22','2021-04-26','2021-04-28','2021-05-02','2021-05-04','2021-05-10','2021-05-16','2021-05-20','2021-05-27','2021-05-31','2021-06-02','2021-06-08','2021-06-10','2021-06-14','2021-06-17','2021-06-24','2021-06-28','2021-07-01','2021-07-05','2021-07-08','2021-07-13','2021-07-19','2021-07-21'],'InputFormat','yyyy-MM-dd')';

figure()
hold on
plot(Lux_dates_WW,Lux.trueCases,'-o','MarkerFaceColor',[0, 0.4470, 0.7410],'MarkerSize',5,'linewidth',1)
plot(Lux_dates_WW,Lux.predsWW,'-o','MarkerFaceColor',[0.8500, 0.3250, 0.0980],'MarkerSize',5,'linewidth',1)
plot(Lux_dates_WW,Lux.predsCase,'-o','MarkerFaceColor',[0.9290, 0.6940, 0.1250],'MarkerSize',5,'linewidth',1)
grid on
legend(["Case numbers (data)","7d Projection (wastewater)","7d Projection (cases)"],'fontsize',14,'Position',[0.536160714285715 0.794642857142857 0.370535714285714 0.129761904761905])
ax = gca;
ax.XTick = datetime(["2020-03-01","2020-05-01","2020-07-01","2020-09-01","2020-11-01","2021-01-01","2021-03-01","2021-05-01","2021-07-01"]);
datetick('x','dd/mm','keepticks', 'keeplimits')
xlim(datetime(["2020-02-24","2021-07-24"]))
ax.FontSize = 14; 
xlabel("Dates 2020-21",'fontsize',18)
ylabel("Cases during 7-days window",'fontsize',18)

corrcoef_Lux = zeros(2,1);
corrcoef_Lux(1) = corr(Lux.trueCases, Lux.predsWW);
corrcoef_Lux(2) = corr(Lux.predsCase, Lux.predsWW);

%% Fig 2b (Performance)

err_mat = [];

for n = 1 : length(file_list_preds_7)  
    err_mat = [err_mat ; performance(parentdir,file_list_preds_7(n))];
end

% Performance, corrected by population size (per 100,000 inh)
correlation = corrcoef(population,err_mat(:,1));
x1 = 0:0.1:13.5;

figure()
hold on
for k = 1: length(err_mat)
    e1 = errorbar(err_mat(k,1)*100000./population(k),err_mat(k,2)*100000./population(k),err_mat(k,4)*100000./(2*population(k)),err_mat(k,4)*100000./(2*population(k)),err_mat(k,3)*100000./(2*population(k)),err_mat(k,3)*100000./(2*population(k)),'o','DisplayName',names(k),'LineWidth', 0.8);
    set(e1,'Color',c(k,:)) 
    set(e1,'MarkerEdgeColor',c(k,:))
    set(e1,'MarkerFaceColor',c(k,:))
end
plot(x1,x1,'--','DisplayName',"1:1","color",[128 128 128]/256)
legend('Location','Northwest','fontsize',14,'NumColumns',3) 
xlabel(["Average standardised error per 100,000 inh.";"(7-days projections from case numbers)"],'fontsize',18)
ylabel(["Average standardised error per 100,000 inh.";"(7-days projections from wastewater data)"],'fontsize',18)
xlim([0,12]) 
ylim([0,25])


%% Fig 2c (Error vs projection time horizon)

winLengthPlot()


%% Fig 2d (Resurgence Zoom-ins)
% Zoom-in into epidemic resurgence data and projections

objFig2d = resurgence(fileparts(pwd),file_list_preds_7 ,names1);

figure('position',[10 10 1200 350]) ;

p = panel();
p.pack(2, 6);
p.de.margin = 9;
p.de.marginbottom = 15;
p.de.marginleft = 3;
p.margin = [18 15 5 15];

p(1,1).select();
objFig2d.bar1()
title(names(1) + ", 2020",'fontweight','normal','fontsize',13)
axis square

p(1,2).select();
objFig2d.kit2()
title(names(2) + ", 2021",'fontweight','normal','fontsize',13)
axis square

p(1,3).select();
objFig2d.kra1()
title(names(3) + ", 2020",'fontweight','normal','fontsize',13)
axis square

p(1,4).select();
objFig2d.lau1()
title(names(4) + ", 2020",'fontweight','normal','fontsize',13)
axis square

p(1,5).select();
objFig2d.lju1()
title(names(5) + ", 2020",'fontweight','normal','fontsize',13)
axis square

p(1,6).select();
objFig2d.lux3()
title(names(6) + ", 2020",'fontweight','normal','fontsize',13)
axis square

p(2,1).select();
objFig2d.mil1()
title(names(7) + ", 2020",'fontweight','normal','fontsize',13)
axis square

p(2,2).select();
objFig2d.net1()
ttl = title("NLD, 2020",'fontweight','normal','fontsize',13);
ttl.Units = 'Normalize'; 
ttl.Position(1) = 0.65;
axis square

p(2,3).select();
objFig2d.osh1()
title(names(9) + ", 2021",'fontweight','normal','fontsize',13)
axis square

p(2,4).select();
objFig2d.ral1()
title(names(10) + ", 2021",'fontweight','normal','fontsize',13)
axis square

p(2,5).select();
objFig2d.rie1()
title("Riera dlB, 2020",'fontweight','normal','fontsize',13)
axis square

p(2,6).select();
objFig2d.zur2()
title(names(12) + ", 2021",'fontweight','normal','fontsize',13)
axis square

legend(["Case numbers (data)","7d Projection (wastewater)","7d Projection (cases)"],'fontsize',12,'Location','SouthOutside','Orientation','horizontal','Box','off')
% Legend to be finally moved manually

p.ylabel('Cases during 7-days window');
p.xlabel('Dates');
set(gca,'FontSize',12)


