%% Supplementary figures for CoWWAn (SEIR_WW_EKF) paper

% The code has been developed and maintained by Atte Aalto, Daniele
% Proverbio and Francoise Kemp
% Affiliation: Luxembourg Centre for Systems Biomedicine
% Year: 2021
% References: "Model-based assessment of COVID-19 epidemic dynamics by
% wastewater sampling"

% Rename the figures after establishing the final order

clc;
close all;
clear;

%% Global variables

% Considered countries
names = ["Barcelona PdL";"Kitchener";"Kranj";"Lausanne";"Ljubljana";"Luxembourg";"Milwaukee";"Netherlands";"Oshkosh";"Raleigh";"Riera de la Bisbal";"Zurich"]; 
acronym = ["BPL";"KIT";"KRA";"LAU";"LJU";"LUX";"MIL";"NLD";"OSH";"RAL";"RDB";"ZUR"];
population = [2000000;242000;40000;240000;280000;610000;615934;17280000;78300;460000;100000;450000];

% Parent directory (this is for results abut predictions, for Fig. 2)
[parentdir,~,~]=fileparts(pwd);

% Data input
names1 = ["Barcelona";"Kitchener";"Kranj";"Lausanne";"Ljubljana";"Luxembourg";"Milwaukee";"Netherlands";"Oshkosh";"Raleigh";"Riera_de_la_Bisbal";"Zurich"]; 

% Prediction outputs
file_list_preds_7 = ["pred7days_Barcelona";"pred7days_Kitchener";"pred7days_Kranj";"pred7days_Lausanne";"pred7days_Ljubljana";"pred7days_Luxembourg";"pred7days_Milwaukee";"pred7days_Netherlands";"pred7days_Oshkosh";"pred7days_Raleigh";"pred7days_Riera_de_la_bisbal";"pred7days_Zurich"];
file_list_preds_14 = ["pred14days_Barcelona";"pred14days_Kitchener";"pred14days_Kranj";"pred14days_Lausanne";"pred14days_Ljubljana";"pred14days_Luxembourg";"pred14days_Milwaukee";"pred14days_Netherlands";"pred14days_Oshkosh";"pred14days_Raleigh";"pred14days_Riera_de_la_bisbal";"pred14daysrich"];

% Reconstruction outputs
file_list_rec = ["reconstruction_Barcelona"; "reconstruction_Kitchener"; "reconstruction_Kranj"; "reconstruction_Lausanne"; "reconstruction_Ljubljana"; "reconstruction_Luxembourg"; "reconstruction_Milwaukee"; "reconstruction_Netherlands"; "reconstruction_Oshkosh"; "reconstruction_Raleigh";"reconstruction_Riera_de_la_bisbal";"reconstruction_Zurich"];

c = colormap(); 


%% Supplementary Fig 1 and 2

ww_for_all_countries(parentdir)

%% Supplementary Figs. 3-15

%Generated directly in the file articleReproduction/main.m

%% Supplementary Fig 16
% Zoom-in into epidemic resurgence data and projections (the remaining
% ones)

objFig2d = resurgence(fileparts(pwd),file_list_preds_7 ,names1);

fig = figure('position',[10 10 900 900]) ;

subplot(4, 4, 1) ;
objFig2d.bar2()
title(names(1) + ", 2021",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 2) ;
objFig2d.bar3()
ttl = title("BPL, 2021",'fontweight','normal','fontsize',12);
ttl.Units = 'Normalize'; 
ttl.Position(1) = 0.68;
axis square

subplot(4, 4, 3) ;
objFig2d.kit1()
title(names(2) + ", 2021",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 4) ;
objFig2d.lux1()
title(names(6) + ", 2020",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 5) ;
objFig2d.lux2()
title(names(6) + ", 2020",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 6) ;
objFig2d.lux4()
title(names(6) + ", 2021",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 7) ;
objFig2d.mil2()
title(names(7) + ", 2021",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 8) ;
objFig2d.net2()
ttl = title("NLD, 2020",'fontweight','normal','fontsize',12);
ttl.Units = 'Normalize'; 
ttl.Position(1) = 0.73;
axis square

subplot(4, 4, 9) ;
objFig2d.net3()
ttl = title("NLD, 2021",'fontweight','normal','fontsize',12);
ttl.Units = 'Normalize'; 
ttl.Position(1) = 0.7;
axis square

subplot(4, 4, 10) ;
objFig2d.rie2()
title("Riera dlB, 2021",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 11) ;
objFig2d.rie3()
title("Riera dlB, 2020",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 12) ;
objFig2d.zur1()
title(names(12) + ", 2020",'fontweight','normal','fontsize',12)
axis square

subplot(4, 4, 13) ;
objFig2d.zur3()
title(names(12) + ", 2021",'fontweight','normal','fontsize',12)
axis square

legend(["Case numbers"+newline+"(data)","7d Projection"+newline+"(wastewater)","7d Projection"+newline+"(cases)"],'fontsize',12,'Box','off','Position',[0.347056067996021 0.114995846225865 0.133221850613155 0.159077809798271])

han=axes(fig,'visible','off');
han.XLabel.Visible='on';
han.YLabel.Visible='on';
ylabel(han,'Cases during 7d window','fontsize',14);
xlabel(han,'Dates','fontsize',14);


%% Supplementary Fig 17
% All average errors against projection time frame, for projections
% obtained from case numbers, wastewater data, or both combined

proj = load('../results/winLengthResults.mat');

% Projections based on case numbers
figure('position',[1,1,800,800])
subplot(2,2,1)
hold on
for k =1 : size(proj.wwErsMean,1)
    plot(1:size(proj.wwErsMean,2),proj.caseErsMean(k,1:end),'color',c(k,:),'linewidth',1,'DisplayName',names(k))
end
xline(7, '--')
xline(14, '-.')
xlim([0,21])
title("Projections from case numbers",'fontsize',14)
ylabel("Average standardised error per 100,000 inh.",'fontsize',14)
xlabel("Time  ahead (d)",'fontsize',14)

% Projections based on wastewater data
subplot(2,2,2)
hold on
for k =1 : size(proj.wwErsMean,1)
    plot(1:size(proj.wwErsMean,2),proj.wwErsMean(k,1:end).^0.5,'color',c(k,:),'linewidth',1,'DisplayName',names(k))
end
xline(7, '--')
xline(14, '-.')
xlim([0,21])
title("Projections from wastewater data",'fontsize',14)
ylabel("Average standardised error per 100,000 inh.",'fontsize',14)
xlabel("Time  ahead (d)",'fontsize',14)

% Projections based on both data combinend
subplot(2,2,3)
hold on
for k =1 : size(proj.wwErsMean,1)
    plot(1:size(proj.wwErsMean,2),proj.bothErsMean(k,1:end).^0.5,'color',c(k,:),'linewidth',1,'DisplayName',names(k))
end
legend('position',[0.221,-0.113,1,1],'fontsize',11,'NumColumns',2)
xline(7, '--','DisplayName','7 days')
xline(14, '-.','DisplayName','14 days')
xlim([0,21])
title("Projections from both data combined",'fontsize',14)
ylabel("Average standardised error per 100,000 inh.",'fontsize',14)
xlabel("Time  ahead (d)",'fontsize',14)
