function cmap = colormap()

cmap = [0 0 0     % inverted jet colormap
    155 155 155
    15 0 184
    0 79 255
    7 173 255
    0 255 120
    25 255 255
    165 42 42
    255 0 0
    255 104 2
    218 165 32
    255 220 0]/256; 

end