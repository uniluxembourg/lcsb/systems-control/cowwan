% Combination plot (case reconstruction + Reff) for Luxembourg

function comboPlot(parentdir)
load([parentdir+"/reconstructionResults/reconstruction_Luxembourg.mat"])
reff_min = readtable([parentdir+"/reconstructionResults/rt-estimate.csv"]);
load(['../parameters/params_Luxembourg.mat'])
labs = {'01/03',' ','01/05',' ','01/07',' ','01/09',' ','01/11',' ','01/01',' ','01/03',' ','01/05',' ','01/07',' ','01/09',' ','01/11',' '};

figure
subplot(300,1,1:198)
hold on; grid on;
Yaux = movmean(caseEstimate,[6 0]);
Ysdaux = movmean(Ysd(1,:).^.5,[6 0]);

dark_num = zeros(size(Yaux));
for m = 1:params.darkNumber(1,2)
    dark_num(m) = params.darkNumber(1,1);
end
for m = params.darkNumber(1,2):length(dark_num)
    dark_num(m) = params.darkNumber(2,1);
end
Yaux_total = dark_num .* Yaux;

plot(Yaux,'r','LineWidth',1.5)
plot(Yaux_total,'b','LineWidth',1.5)
plot(movmean(YC,[6 0]),'-.k','LineWidth',1.5)
h1 = fill([1:length(Yaux) fliplr(1:length(Yaux))],[max(Yaux-2*Ysdaux,0) fliplr(Yaux+2*Ysdaux)],[1,.93,.93],'EdgeColor','none');
plot(Yaux,'r','LineWidth',1.5)
plot(Yaux_total,'b','LineWidth',1.5)
plot(movmean(YC,[6 0]),'-.k','LineWidth',1.5)
set(gca,'layer','top');
set(gca,'FontSize',14)
%set(h1,'facealpha',.3)
hL = legend({"Estimated detected cases",'Estimated total cases','Data','2SD envelope'},'Location','Northeast','FontSize',13);
set(hL, 'Position', [0.69 0.75 0.08 0.17]);
yticks(0:300:1200)
xticks(firsts)
xticklabels({})
ylabel('Daily new cases','FontSize',16)
axis([0 523 0 1250])

subplot(300,1,202:300)
hold on
grid
plot([zeros(26,1); reff_min.R_t_estimate(2:end-3)],'k','LineWidth',1)
plot(ReffWW,'r','LineWidth',1.5)
h1 = fill([1:length(ReffWW) fliplr(1:length(ReffWW))],[ReffWW-2*ReffWWSD fliplr(ReffWW+2*ReffWWSD)],[1,.93,.93],'EdgeColor','none');
plot([1 length(ReffCase)+10],[1 1],'--k')
plot([zeros(26,1); reff_min.R_t_estimate(2:end-3)],'k','LineWidth',1)
plot(ReffWW,'r','LineWidth',1.5)
set(gca,'layer','top');
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
xtickangle(0)
yticks(.5:.5:2)
hL = legend({'From Lux Health Ministry',"Estimated from WW data"},'Location','Northeast','FontSize',13);
set(hL, 'Position', [0.69 0.30 0.08 0.08]);
ylabel('R_{eff}','FontSize',16)
xlabel('Dates 2020-21','FontSize',16) 
axis([0 523 0.5 2.5])
box off

end
