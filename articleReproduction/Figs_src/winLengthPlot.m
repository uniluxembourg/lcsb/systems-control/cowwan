function winLengthPlot()

load('../results/winLengthResults.mat');

figure()
hold on
 
%Define the error bar
erb = [-.12 .12 0 0 -.12 .12; -1 -1 -1 1 1 1];

wwmean = zeros(1,21);
casemean = zeros(1,21);
bothmean = zeros(1,21);

wwhi = zeros(1,21);
casehi = zeros(1,21);
bothhi = zeros(1,21);

wwlo = zeros(1,21);
caselo = zeros(1,21);
bothlo = zeros(1,21);


for jw = 1:21
    caseEE = [];
    wwEE = [];
    bothEE = [];
    for jr = [1:2 4:12]
        %Error over the full jw-day horizon
        caseEE = [caseEE; caseErsTot{jr,jw}];
        wwEE = [wwEE; wwErsTot{jr,jw}];
        bothEE = [bothEE; bothErsTot{jr,jw}];
        
        %Error for only the jw^th day ahead
%         caseEE = [caseEE; caseErs{jr,jw}];
%         wwEE = [wwEE; wwErs{jr,jw}];
%         bothEE = [bothEE; bothErs{jr,jw}];
                
    end
    
    caseEE = sort(caseEE,'ascend');
    wwEE = sort(wwEE,'ascend');
    bothEE = sort(bothEE,'ascend');
    
    
    
    wwmean(jw) = mean(wwEE);
    casemean(jw) = mean(caseEE);
    bothmean(jw) = mean(bothEE);
    
    wwlo(jw) = wwEE(1+floor(.1*length(wwEE)));
    caselo(jw) = caseEE(1+floor(.1*length(caseEE)));
    bothlo(jw) = bothEE(1+floor(.1*length(bothEE)));
    
    wwhi(jw) = wwEE(floor(.9*length(wwEE)));
    casehi(jw) = caseEE(floor(.9*length(caseEE)));
    bothhi(jw) = bothEE(floor(.9*length(bothEE)));
    
end
    

plot([.7 1:21 21.3],[wwmean(1) wwmean wwmean(end)],'LineWidth',1.5,'color',[0.8500, 0.3250, 0.0980])
plot([.7 1:21 21.3],[casemean(1) casemean casemean(end)],'LineWidth',1.5,'color',[0.9290, 0.6940, 0.1250])
plot([.7 1:21 21.3],[bothmean(1) bothmean bothmean(end)],'LineWidth',1.5,'color',[100,149,237]/256)


for jt = 1:5:21
    plot(jt-.2 + erb(1,:), (wwlo(jt)+wwhi(jt))/2 + (wwhi(jt)-wwlo(jt))/2*erb(2,:),'LineWidth',1.5,'color',[0.8500, 0.3250, 0.0980])
    plot(jt + erb(1,:), (caselo(jt)+casehi(jt))/2 + (casehi(jt)-caselo(jt))/2*erb(2,:),'LineWidth',1.5,'color',[0.9290, 0.6940, 0.1250])
    plot(jt+.2 + erb(1,:), (bothlo(jt)+bothhi(jt))/2 + (bothhi(jt)-bothlo(jt))/2*erb(2,:),'LineWidth',1.5,'color',[100,149,237]/256)

end
 

legend(["Wastewater", "Cases","Combined"],'fontsize',14,'location','northwest' )
ylabel("Average standardised error per 100,000 inh.",'fontsize',18)
xlabel("Projection horizon (days)",'fontsize',18)
xlim([0,21.9])
hold off

end