%% Correlations of cases and smoothed WW data to be used in plotting

regions = {'Barcelona','Kitchener','Kranj','Lausanne','Ljubljana','Luxembourg','Milwaukee','Netherlands','Oshkosh','Raleigh','Riera_de_la_bisbal','Zurich'};
addpath('../SEIRWWfiles/')

for jr = 1:length(regions)

    region = regions{jr};
    load(['../parameters/params_' region '.mat'])
    
   
    WWinds = find(YW > -.0001);
    Tlim = max(WWinds);
    YWsm = zeros(1,Tlim);
    YWsm(1) = YW(1);
    discount = .8;
    for jd = 2:Tlim
        if YW(jd)<0
            YWsm(jd) = YWsm(jd-1);
            discount = .8*discount;
        else
            YWsm(jd) = discount*YWsm(jd-1) + (1-discount)*YW(jd);
            discount = .8;
        end
    end
       
    YCsm = movmean(YC,[6 0]);
    
    WWinds = find(YWsm > -.0001);
    
    corrSmoothedData(jr) = (YWsm(WWinds) - mean(YWsm(WWinds)))*(YC(WWinds) - mean(YC(WWinds)))'/norm(YWsm(WWinds)-mean(YWsm(WWinds)))/norm(YC(WWinds)-mean(YC(WWinds)));

end


%% Correlations of raw data

% regions = {'Barcelona','Kitchener','Kranj','Lausanne','Ljubljana','Luxembourg','Milwaukee','Netherlands','Oshkosh','Raleigh','Riera_de_la_bisbal','Zurich'};
% addpath('./SEIRWWfiles/')
% 
% for jr = 1:length(regions)
% 
%     region = regions{jr};
%     load(['./parameters/params_' region '.mat'])
%     
%    
%     WWinds = find(YW > -.0001);
%     
%     dataCors(jr) = (YW(WWinds) - mean(YW(WWinds)))*(YC(WWinds) - mean(YC(WWinds)))'/norm(YW(WWinds)-mean(YW(WWinds)))/norm(YC(WWinds)-mean(YC(WWinds)));
% end


