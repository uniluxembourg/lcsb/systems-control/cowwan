%% Choose the region by uncommenting a line and load data and fitted parameters

%region = 'Barcelona';  
%region = 'Kitchener';  
%region = 'Kranj';      
%region = 'Lausanne';  
%region = 'Ljubljana';   
%region = 'Luxembourg';
%region = 'Milwaukee';
%region = 'Netherlands';
%region = 'Oshkosh';    
%region = 'Raleigh';     
region = 'Riera_de_la_bisbal'; 
%region = 'Zurich';     

load(['../parameters/params_' region '.mat'])
addpath('../SEIRWWfiles/')

%% Plot the wastewater data estimate using only the case data
params.RW = params.RW0/10;
[Yest, ~, ~, ReffCase, ReffCaseSD] = SEIR_WW(params,YC,YW,C,[true,false],1000,firsts,labs,'big');
wwEstimate = Yest(2,:);

%% Plot the case data estimate using only the wastewater data
params.RW = params.RW0/10;
[Yest, ~, ~, ReffWW, ReffWWSD, Ysd] = SEIR_WW(params,YC,YW,C,[false,true],1000,firsts,labs,'big');
caseEstimate = Yest(1,:);
save(['../results/reconstruction_' params.region '.mat'],'wwEstimate','caseEstimate','ReffCase','ReffCaseSD','ReffWW','ReffWWSD','YC','YW','Ysd')

%% Plot the case data estimate using only the interpolated wastewater data
YWip = WWinterpol(YW);
params.RW = params.RW0/10;
Yest = SEIR_WW(params,YC,YWip,C,[false,true],1000,firsts,labs,true);

%% 7-day ahead predictions

winLength = 7;
WWinds = find(YW>-.5);
excl = sum(WWinds+winLength > length(YC));
WWinds = WWinds(1:end-excl);
trueCases = zeros(length(WWinds),1);
for jd = 1:length(WWinds)
    trueCases(jd) = sum(YC(WWinds(jd)+(1:winLength)));
end
prediction
deltaR = ReffCase(WWinds+winLength) - ReffCase(WWinds);
save(['../results/pred' num2str(winLength) 'days_' params.region '.mat'],'trueCases','predsCase','predsWW','predsWWip','predsBoth','deltaR')

%% 14-day predictions

winLength = 14;
WWinds = find(YW>-.5);
excl = sum(WWinds+winLength > length(YC));
WWinds = WWinds(1:end-excl);
trueCases = zeros(length(WWinds),1);
for jd = 1:length(WWinds)
    trueCases(jd) = sum(YC(WWinds(jd)+(1:winLength)));
end
prediction
deltaR = ReffCase(WWinds+winLength) - ReffCase(WWinds);
save(['../results/pred' num2str(winLength) 'days_' params.region '.mat'],'trueCases','predsCase','predsWW','predsWWip','predsBoth','deltaR')


%% Look at the errors

ersCase = 1e5/params.N*(trueCases-predsCase)./trueCases.^.5;
ersWW = 1e5/params.N*(trueCases-predsWW)./trueCases.^.5;
ersWWip = 1e5/params.N*(trueCases-predsWWip)./trueCases.^.5;
ersBoth = 1e5/params.N*(trueCases-predsBoth)./trueCases.^.5;

iii = 1:length(trueCases);
iii = iii(find(trueCases > 0));

disp(['* * * ERRORS for ' params.region ' (' num2str(winLength) '-day window) * * *'])
disp(['Case data: ' num2str(round(mean(abs(ersCase(iii))),2)) ' (SD: '  num2str(round(var(abs(ersCase(iii)))^.5,2)) ') ' ' ('  num2str(round(100*(mean(abs(ersCase(iii)))-mean(abs(ersCase(iii))))/mean(abs(ersCase(iii))),1)) '%)'])
disp(['WW data:   ' num2str(round(mean(abs(ersWW(iii))),2)) ' (SD: '  num2str(round(var(abs(ersWW(iii)))^.5,2)) ') ' ' ('  num2str(round(100*(mean(abs(ersWW(iii)))-mean(abs(ersCase(iii))))/mean(abs(ersCase(iii))),1)) '%)'])
disp(['WWip data: ' num2str(round(mean(abs(ersWWip(iii))),2)) ' (SD: '  num2str(round(var(abs(ersWWip(iii)))^.5,2)) ') ' ' ('  num2str(round(100*(mean(abs(ersWWip(iii)))-mean(abs(ersCase(iii))))/mean(abs(ersCase(iii))),1)) '%)'])
disp(['All data:  ' num2str(round(mean(abs(ersBoth(iii))),2)) ' (SD: '  num2str(round(var(abs(ersBoth(iii)))^.5,2)) ') ' ' ('  num2str(round(100*(mean(abs(ersBoth(iii)))-mean(abs(ersCase(iii))))/mean(abs(ersCase(iii))),1)) '%)'])


%% Prediction time series

figure('Position',[100,200,1200,450]); hold on; grid on;
plot(WWinds,trueCases,'-s','LineWidth',2,'MarkerFaceColor',[0,.45,.74],'MarkerSize',5)
plot(WWinds,predsWW,'-o','LineWidth',2,'MarkerFaceColor',[.85,.33,.1],'MarkerSize',5)
plot(WWinds,predsCase,'-d','LineWidth',2,'MarkerFaceColor',[.93,.69,.13],'MarkerSize',5)
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
legend({'Case numbers (data)','7d Projection (wastewater)','7d projection (cases)'},'Location','Northeast','FontSize',14)
ylabel('Cases during 7-day window','FontSize',16)
if max(strcmp(params.region,{'Kitchener','Raleigh'}))
    xlabel('Dates 2021','FontSize',16) 
else
    xlabel('Dates 2020-21','FontSize',16)  
end
title('d','FontSize',20)

%% R_eff

figure('Position',[100,200,1200,450])
plot(ReffCase,':k','LineWidth',2)
hold on
grid
plot(ReffWW,'r','LineWidth',2)
h1 = fill([1:length(ReffWW) fliplr(1:length(ReffWW))],[ReffWW-2*ReffWWSD fliplr(ReffWW+2*ReffWWSD)],[1,.75,.75],'EdgeColor','none');
if strcmp(params.region,'Zurich')
    load('huisman_R.mat')
    plot(DD(14:end)-30,RR(14:end),'-.','LineWidth',2,'Color',[.5 0 .5])
end
plot(ReffCase,':k','LineWidth',2)
plot(ReffWW,'r','LineWidth',2)
plot([1 length(ReffCase)+10],[1 1],'k')

set(gca,'layer','top');
set(gca,'FontSize',14)
set(h1,'facealpha',.3)
xticks(firsts)
xticklabels(labs)
if strcmp(params.region,'Zurich')
    legend({'Estimated from case data','Estimated from wastewater data','2SD envelope','From Huisman et al.'},'Location','Northeast','FontSize',14)
else
    legend({'Estimated from case data','Estimated from wastewater data','2SD envelope'},'Location','Northeast','FontSize',14)
end
ylabel('R_{eff}','FontSize',16)
if max(strcmp(params.region,{'Kitchener','Raleigh'}))
    xlabel('Dates 2021','FontSize',16) 
else
    xlabel('Dates 2020-21','FontSize',16)  
end
title('c','FontSize',20)


%% Longer term predictions (for Luxembourg)

jcase = 2; 

%Dates 1/10/20, 1/11/20, 1/12/20, 1/2/21, 1/4/21
dds = [219 250 280 342 401];
titles = {'a','b';'c','d';'e','f';'g','h';'i','j'};
predDay = dds(jcase);
params.RW = params.RW0/10;
Y = YC;

%Using case data
[~, XendC, PC] = SEIR_WW(params,YC,YW,C,[true, false],predDay,firsts,labs,false);
[Y0, err] = SEIR_WW_FWD(XendC,C,PC,predDay+1,params,330);
Yc0 = cumsum([Y(1:predDay) Y0(1,:)]);
Xaux = XendC;
Xaux(7) = XendC(7) + PC(7,7).^.5;
Y1 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Yc1 = cumsum([Y(1:predDay) Y1(1,:)]);
Xaux(7) = XendC(7) + 2*PC(7,7).^.5;
Y2 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Yc2 = cumsum([Y(1:predDay) Y2(1,:)]);
Xaux(7) = XendC(7) - PC(7,7).^.5;
Ym1 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Ycm1 = cumsum([Y(1:predDay) Ym1(1,:)]);
Xaux(7) = XendC(7) - 2*PC(7,7).^.5;
Ym2 = SEIR_WW_FWD(Xaux,C,PC,predDay+1,params,330);
Ycm2 = cumsum([Y(1:predDay) Ym2(1,:)]);

%Using WW data
[~, XendW, PW] = SEIR_WW(params,YC,YW,C,[false, true],predDay,firsts,labs,false);
[YWW0, err] = SEIR_WW_FWD(XendW,C,PW,predDay+1,params,330);
Yw0 = cumsum([Y(1:predDay) YWW0(1,:)]);
Xaux = XendW;
Xaux(7) = XendW(7) + PW(7,7).^.5;
Y1 = SEIR_WW_FWD(Xaux,C,PW,predDay+1,params,330);
Yw1 = cumsum([Y(1:predDay) Y1(1,:)]);
Xaux(7) = XendW(7) + 2*PW(7,7).^.5;
Y2 = SEIR_WW_FWD(Xaux,C,PW,predDay+1,params,330);
Yw2 = cumsum([Y(1:predDay) Y2(1,:)]);
Xaux(7) = XendW(7) - PW(7,7).^.5;
Ym1 = SEIR_WW_FWD(Xaux,C,PW,predDay+1,params,330);
Ywm1 = cumsum([Y(1:predDay) Ym1(1,:)]);
Xaux(7) = XendC(7) - 2*PW(7,7).^.5;
Ym2 = SEIR_WW_FWD(Xaux,C,PW,predDay+1,params,330);
Ywm2 = cumsum([Y(1:predDay) Ym2(1,:)]);


figure('Position',[400,200 560 380]); grid; hold on;

%h1=fill([1:length([Y(1:predDay) Y1(1,:)]) fliplr(1:length([Y(1:predDay) Y1(1,:)]))],[movmean([Y(1:predDay) Y2(1,:)],[6,0]) fliplr(movmean([Y(1:predDay) Ym2(1,:)],[6,0]))],[1,.83,.83],'EdgeColor','none');
%h2=fill([1:length([Y(1:predDay) Y1(1,:)]) fliplr(1:length([Y(1:predDay) Y1(1,:)]))],[movmean([Y(1:predDay) Y1(1,:)],[6,0]) fliplr(movmean([Y(1:predDay) Ym1(1,:)],[6,0]))],[.83,.83,1],'EdgeColor','none');

plot(movmean([Y(1:predDay) Y0(1,:)],[6,0]),'b--','LineWidth',2)
plot(movmean([Y(1:predDay) YWW0(1,:)],[6,0]),'r','LineWidth',2)
plot(movmean(Y,[6,0]),'k:','LineWidth',2)
plot(predDay,sum(Y(predDay-6:predDay))/7,'^k','MarkerFaceColor','g','MarkerSize',9)
xlabel('Date','FontSize',16)
ylabel('Daily cases','FontSize',16)
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
legend({'Projection (Case data)','Projection (WW data)',['Data']},'Location','Northwest','FontSize',14)
axis([1 554 0 800])
title(titles{jcase,1},'FontSize',20)
if jcase == 2
    axis([1 554 0 3000])
end

figure('Position',[400,200 560 380]); grid on; hold on;
plot(Yc0,'b--','LineWidth',2)
plot(Yw0,'r','LineWidth',2)
plot(cumsum(Y),'k:','LineWidth',2)
h1=fill([predDay:length(Yc0),fliplr(predDay:length(Yc0))],[Yc0(predDay),Ycm2(predDay+1:length(Ycm2)),fliplr(Yc2(predDay+1:length(Yc2))),Yc0(predDay)],[.83,.83,1],'EdgeColor','none');
h2=fill([predDay:length(Yw0),fliplr(predDay:length(Yw0))],[Yw0(predDay),Ywm2(predDay+1:length(Ywm2)),fliplr(Yw2(predDay+1:length(Yc2))),Yw0(predDay)],[1,.83,.83],'EdgeColor','none');
plot(Yc0,'b--','LineWidth',2)
plot(Yw0,'r','LineWidth',2)
plot(cumsum(Y),'k:','LineWidth',2)
set(h1,'facealpha',.3)
set(h2,'facealpha',.3)
plot(predDay,Yc0(predDay),'^k','MarkerFaceColor','g','MarkerSize',9)
set(gca,'layer','top');
xlabel('Date','FontSize',16)
ylabel('Cumulative cases','FontSize',16)
set(gca,'FontSize',14)
xticks(firsts)
xticklabels(labs)
legend({'Projection (Case data)','Projection (WW data)','Data'},'Location','Northwest','FontSize',14)
axis([1 554 0 14e4])
title(titles{jcase,2},'FontSize',20)
if jcase == 2
    axis([1 554 0 2.2e5])
end








